from setuptools import setup, find_packages

setup(
    name='ai3-gunicorn-utils',
    version='0.1',
    description='Misc utilities to support running gunicorn in the ai3 system.',
    author='A/I',
    author_email='info@autistici.org',
    install_requires=[],
    packages=find_packages(),
)

import re

from ai3_gunicorn_utils import SSL_CLIENT_CN_HEADER
WSGI_SSL_CLIENT_CN_HEADER = 'HTTP_' + SSL_CLIENT_CN_HEADER.upper()


def _abort(start_response, code, code_str):
    hdrs = [('Content-Type', 'text/plain')]
    start_response(code, code_str)
    return [code_str]


def _compile_config(config):
    # Turn config dictionaries into path/cn compiled regexp tuples.
    def _compile_entry(e):
        return (re.compile(e['path']), re.compile(e['cn']))
    return filter(_compile_entry, config)


class TLSAuthMiddleware(object):

    def __init__(self, next_app, config):
        self._next_app = next_app
        self._config = _compile_config(config)

    def __call__(self, environ, start_response):
        path = environ['SCRIPT_NAME'] + environ['PATH_INFO']
        client_cn = environ.get(WSGI_SSL_CLIENT_CN_HEADER)
        if not client_cn:
            return _abort(start_response, 401, 'Unauthorized')
        for path_rx, cn_rx in self._config:
            if path_rx.match(path) and cn_rx.match(client_cn):
                return self._next_app(environ, start_response)
        return _abort(start_response, 403, 'Forbidden')


def tls_auth_wrap(next_app):
    """Wrap a WSGI application with TLSAuthMiddleware.

    Configuration is loaded from the file specified by the environment
    variable TLS_AUTH_CONFIG. If the variable is not defined or empty,
    TLS authentication is disabled.

    The configuration file should be a Python file defining a single
    ACL variable, a list of {path, cn} dictionaries containing ACL
    definitions.

    """
    config_file = os.getenv('TLS_AUTH_CONFIG')
    if not config_file:
        return next_app

    config_ctx = {'ACL': []}
    execfile(config_file, config_ctx)
    config = config_ctx['ACL']
    return TLSAuthMiddleware(next_app, config)


def init_flask_app(flask_app):
    """Add TLSAuthMiddleware to a Flask application."""
    app.wsgi_app = tls_auth_wrap(app.wsgi_app)

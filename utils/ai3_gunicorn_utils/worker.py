from ai3_gunicorn_utils import SSL_CLIENT_CN_HEADER
from gunicorn.workers.ggevent import GeventWorker as worker_base


class TLSAuthWorker(worker_base):
    """Custom gunicorn worker that sets SSL_CLIENT_CN_HEADER.

    Based on the gevent worker.

    """
    def handle_request(self, listener, req, client, addr):
        # Need to ensure we've shaken hands.
        client.do_handshake()

        subject = dict(client.getpeercert().get('subject')[0])

        # Remove any SSL_CLIENT_CN_HEADER that might have been already
        # present in the incoming request, and set our own value.
        headers = list(filter(lambda x: x[0] != SSL_CLIENT_CN_HEADER, req.headers))
        headers.append((SSL_CLIENT_CN_HEADER, subject.get('commonName')))
        req.headers = headers

        super(TLSAuthWorker, self).handle_request(listener, req, client, addr)

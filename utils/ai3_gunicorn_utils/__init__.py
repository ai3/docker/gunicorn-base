
# The special header that we set in our custom gunicorn worker, that
# holds the client certificate's subject CN.
SSL_CLIENT_CN_HEADER = 'X-SSL-Client-CN'

ai3_gunicorn_utils
===

WSGI middleware and utilities for gunicorn that enable TLS-based
authorization to secure service-to-service communication, in line with
the features provided
by
[ai3/go-common](https://git.autistici.org/ai3/go-common/blob/master/serverutil/tls.go).

To enable the feature for an application using
the
[ai3/docker-gunicorn-base](https://git.autistici.org/ai3/docker-gunicorn-base) image,
follow these steps:

### Setup GUnicorn

In the *gunicorn.conf* file for your application, set up our custom
gunicorn worker (ai3_gunicorn_utils.worker.TLSAuthWorker), and
enable SSL with client certificates:

```yaml
import ssl
worker_class = "ai3_gunicorn_utils.worker.TLSAuthWorker"
keyfile = "/etc/credentials/x509/myservice/server/private_key.pem"
certfile = "/etc/credentials/x509/myservice/server/cert.pem"
ca_certs = "/etc/credentials/x509/myservice/ca.pem"
ssl_version = ssl.PROTOCOL_TLSv1_2
cert_reqs = ssl.CERT_REQUIRED
```

The custom worker is necessary to pass the client certificate
information to the WSGI middleware.

### Setup your application

Configure your WSGI application file to (optionally) initialize TLS
authentication if the TLS_AUTH_CONFIG environment variable is defined:

```python
from ai3_gunicorn_utils.tls_auth import tls_auth_wrap
from myapplication import create_wsgi_app

application = tls_auth_wrap(create_wsgi_app())
```

or, if you are using Flask for instance:

```python
from ai3_gunicorn_utils.tls_auth import init_flask_app
from myapplication import create_app

application = create_app()
init_flask_app(application)
```

### Configure TLS authentication

The file pointed to by TLS_AUTH_CONFIG must be a Python file, defining
one top-level variable `ACL`, which contains a list of access-control
patterns. For each requests, at least one of these patterns must match
the request parameters. ACL patterns are made of two regular
expressions, one that should match the HTTP request path, another that
should match the client certificate's subject CN. They are expressed
as Python dictionaries with *path* and *cn* attributes, for instance:

```python
ACL = [
  {"path": "^/api/", "cn": "^other-service$"},
  {"path": "^/metrics", "cn": ".*"},
]
```


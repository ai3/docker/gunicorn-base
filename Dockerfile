FROM registry.git.autistici.org/ai3/docker/s6-base:master

COPY build.sh /tmp/build.sh
COPY utils /tmp/utils
COPY conf/ /etc/

RUN /tmp/build.sh && rm /tmp/build.sh


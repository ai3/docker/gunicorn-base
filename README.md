docker-gunicorn-base
===

Base image with [gunicorn](http://gunicorn.org), to run Python web applications.
The configuration for the app should be copied by sub-images to /etc/gunicorn.conf
(even though it is a Python file). The name of the application module to run
should be passed as part of the ENTRY\_POINT set by the child image.

### TLS Authentication

This image installs a bit of Python code, required to use TLS
authentication for your application in a standardized fashion (since
it requires a custom GUnicorn worker).

To read how to do so, check the [utils/README.md](utils/README.md)
file.

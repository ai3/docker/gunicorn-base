#!/bin/sh

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="python3-pip python3-setuptools python3-wheel python3-gevent gunicorn3"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

# Install required packages
apt-get -q update
install_packages ${PACKAGES} \
    || die "could not install packages"

# Set up the ai3_gunicorn_utils package.
pip3 install /tmp/utils

# Install gunicorn3 as just 'gunicorn' for compatibility.
ln -s gunicorn3 /usr/bin/gunicorn

# Remove packages used for installation.
#apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
